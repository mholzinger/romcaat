# Create a list of csv files in a folder for doing test of romdumps
# This script utilizes ssconvert from the gnumeric toolset to import csv to
# tabbed .xls

# THIS SCRIPT
prog=${0##*/}

print_usage()
{
  echo "usage: "$prog" [-d path to set of files to build list] [-h help]"
  echo -e "\t\t[-o filename of output.xls]"
  echo -e "\texample: "$prog" -d /path/to/set/ -o output.xls]"
  exit
}

build_list()
{
  folders=("${!1}")
  output="$2"

  if [[ -n "$VERBOSE" ]];then
    echo "debug folders" "${folders[@]}"
    echo "debug output" "$output"
  fi

  work_path=$( mktemp -d )

  header='"Title","MD5","Boots","Playable","Notes"'

  for i in "${folders[@]}"
  do
    file=$(basename "$i"|sed 's/\ /-/g')
    csv_file=$work_path/$file.csv

    if [[ -n "$VERBOSE" ]];then
      echo "debug folder $i"
      echo "debug filename $csv_file"
    fi

    echo "$header" >> $csv_file

    # This block calls ls, pipes ls output to strip any unicode which breaks
    # different versions of gnumeric due to utf-8 support not being
    # supported on GNU gnumeric
    ls -1 "$i" |\
      while read -r file
      do
        ascii_file=$(echo "$file"|iconv -c -f utf-8 -t ascii)

        # earlier in the block we declared a loop where "$i" is the index
        # name of the folder through the list we created. so $i/$file is
        # our file to md5 checksum
        md5_file=$(md5sum "$i/$file"|awk '{print $1}')
        echo \""$ascii_file"\",\""$md5_file"\",\"\",\"\",\"\" >> $csv_file
      done
    done

  # Strip extra xls extension if provided
  excel_file=${output%.xls}.xls

  if [[ -n "$VERBOSE" ]];then
    ssconvert --merge-to=$excel_file $work_path/*.csv
  else
    ssconvert --merge-to=$excel_file $work_path/*.csv 2> /dev/null
  fi

  echo "File successfully written to [$excel_file]"
}


# Main processing loop
while getopts :d:o:hv option
do
  case "${option}" in
    d)
        LIST_PATH=$OPTARG;;
    o)
        OUTPUT=$OPTARG;;
    v)
        export VERBOSE=1;;
    h)
        print_usage
        exit;;
    *)
        echo $prog: illegal option -- ${OPTARG}
        print_usage
        exit;;
  esac
done

if [[ -z "$LIST_PATH" || -z "$OUTPUT" ]];
  then
    echo "Err: Missing a required parameter ([D]irectory path and [O]utput are required)"
    print_usage
fi

# Build an array
folders=("$LIST_PATH"*/)
build_list "folders[@]" $OUTPUT
