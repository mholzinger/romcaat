# romcaat

Computer Assisted Auditing Tools for ROM dumps


```
usage: path2tabs.sh [-d path to set of files to build list]
[-h help] [-o filename of output.xls]
```

Example:

```
path2tabs.sh -d /path/to/set/ -o output.xls
```

File: *output.xls*

| Tab 1 |

| Title | Boots  | Playable | Notes |
|:-:|:-:|:-:|:-:|
| homebrew.nes |   |   |   |
| homebrew[!].nes |   |   |   |
| homebrew (J).nes  |   |   |   |

| Tab 2 |

| Title | Boots  | Playable | Notes |
|:-:|:-:|:-:|:-:|
| testdump.nes |   |   |   |
| testdump (bad).nes |   |   |   |


This script utilizes `ssconvert` from the [**gnumeric**] toolset to import .csv files to tabbed .xls

Mac OSX:

```brew install gnumeric```

Debian:

```sudo apt install gnumeric```
